package com.charurani.core.networking.thread

import io.reactivex.Scheduler

interface PostExecutionThread {
    fun provideScheduler(): Scheduler
}