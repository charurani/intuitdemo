package com.charurani.core.networking

import com.charurani.core.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class NetworkClient @Inject constructor() {

    companion object {
        private var retrofit: Retrofit? = null

        fun <T> create(service: Class<T>?): T {
            if (retrofit == null) {
                retrofit = createRetrofit()
            }
            requireNotNull(retrofit)
            return retrofit?.create(service)?: createRetrofit().create(service)
        }

        private fun createOkHttpClient(): OkHttpClient {
            val builder = OkHttpClient.Builder()
            builder.retryOnConnectionFailure(true)
            if (BuildConfig.ENABLE_HTTP_LOGS) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                builder.addInterceptor(interceptor)
            }
            builder.readTimeout(1, TimeUnit.MINUTES)
            builder.writeTimeout(1, TimeUnit.MINUTES)
            builder.connectTimeout(1, TimeUnit.MINUTES)
            return builder.build()
        }

        private fun createRetrofit(): Retrofit {

            val client = createOkHttpClient()
            val baseUrl = "https://api.thecatapi.com/v1/"
            var builder = Retrofit.Builder()
                .baseUrl(baseUrl)
            val moshiBuilder = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
            builder.addConverterFactory(MoshiConverterFactory.create(moshiBuilder.build()))
            builder = builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
            return builder.build()
        }
    }
}