package com.charurani.core.networking.thread

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class PostExecutionThreadImpl @Inject constructor() : PostExecutionThread {
    override fun provideScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}