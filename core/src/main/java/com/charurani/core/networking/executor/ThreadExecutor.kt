package com.charurani.core.networking.executor

import java.util.concurrent.Executor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val THREAD_POOL_SIZE = 3
const val MAX_THREAD_POOL_SIZE = 3
const val KEEP_ALIVE_TIME = 10L

class ThreadExecutor @Inject constructor() : Executor {

    private var threadPoolExecutor = ThreadPoolExecutor(
        THREAD_POOL_SIZE, MAX_THREAD_POOL_SIZE,
        KEEP_ALIVE_TIME, TimeUnit.SECONDS, LinkedBlockingQueue()
    )

    override fun execute(command: Runnable) {
        threadPoolExecutor.execute(command)
    }
}