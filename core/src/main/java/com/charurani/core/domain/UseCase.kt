package com.charurani.core.domain

import com.charurani.core.networking.thread.PostExecutionThread
import dagger.internal.Preconditions
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor

abstract class UseCase<T, R>(
    private val threadExecutor: Executor,
    private val postExecutionThread: PostExecutionThread,
    protected val disposables: CompositeDisposable = CompositeDisposable()
) {

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    private fun addDisposable(disposable: Disposable) {
        Preconditions.checkNotNull(disposable)
        Preconditions.checkNotNull<CompositeDisposable>(disposables)
        disposables.add(disposable)
    }

    fun execute(params: R): Observable<T> {
        return buildUseCaseLiveData(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.provideScheduler())
    }

    protected abstract fun buildUseCaseLiveData(params: R): Observable<T>
}