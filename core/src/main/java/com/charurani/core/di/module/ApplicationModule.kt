package com.charurani.core.di.module

import android.app.Application
import com.charurani.core.networking.executor.ThreadExecutor
import com.charurani.core.networking.thread.PostExecutionThread
import com.charurani.core.networking.thread.PostExecutionThreadImpl
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import javax.inject.Singleton

@Module
class ApplicationModule(val application: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    fun provideExecutor(executor: ThreadExecutor): Executor {
        return executor
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(postExecutionThreadImpl: PostExecutionThreadImpl): PostExecutionThread {
        return postExecutionThreadImpl
    }

}