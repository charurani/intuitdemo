package com.charurani.core.di.component

import com.charurani.core.di.module.ActivityModule
import com.charurani.core.di.scopes.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
}