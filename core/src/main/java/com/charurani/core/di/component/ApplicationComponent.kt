package com.charurani.core.di.component

import com.charurani.core.di.module.ActivityModule
import com.charurani.core.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun plus(activityModule: ActivityModule?): ActivityComponent?
}