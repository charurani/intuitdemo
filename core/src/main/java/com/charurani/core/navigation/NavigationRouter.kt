package com.charurani.core.navigation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import javax.inject.Inject

class NavigationRouter @Inject constructor() {
    private val params: HashMap<String, Any> = HashMap()
    fun withParam(
        name: String,
        value: String
    ): NavigationRouter? {
        params.put(name, value)
        return this
    }

    fun withParam(
        name: String,
        value: Boolean
    ): NavigationRouter? {
        params.put(name, value)
        return this
    }

    fun withParam(
        name: String,
        value: Long
    ): NavigationRouter? {
        params.put(name, value)
        return this
    }

    fun withParam(
        name: String,
        value: Int
    ): NavigationRouter? {
        params.put(name, value)
        return this
    }

    fun withParam(
        name: String,
        value: Parcelable
    ): NavigationRouter? {
        params.put(name, value)
        return this
    }

    fun <T> navigateTo(initiatingActivity: Activity, landingActivity: Class<T>) {
        val intent: Intent = createIntent(initiatingActivity, landingActivity)
        initiatingActivity.startActivity(intent)
    }

    private fun putParams(bundle: Bundle) {
        for (key in params.keys) {
            val obj = params[key]
            if (obj is String) {
                bundle.putString(key, obj as String?)
            } else if (obj is Long) {
                bundle.putLong(key, (obj as Long?)!!)
            } else if (obj is Int) {
                bundle.putInt(key, (obj as Int?)!!)
            } else if (obj is Parcelable) {
                bundle.putParcelable(key, obj as Parcelable?)
            } else if (obj is Boolean) {
                bundle.putBoolean(key, (obj as Boolean?)!!)
            }
        }
    }

    private fun <T> createIntent(initiatingActivity: Activity, landingActivity: Class<T>): Intent {
        val intent = Intent(initiatingActivity, landingActivity)
        val params = getParams()
        if (params != null) {
            intent.putExtras(params)
        }
        return intent
    }

    private fun getParams(): Bundle? {
        val bundle = Bundle()
        putParams(bundle)
        return bundle
    }
}