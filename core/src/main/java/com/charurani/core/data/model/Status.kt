package com.charurani.core.data.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}