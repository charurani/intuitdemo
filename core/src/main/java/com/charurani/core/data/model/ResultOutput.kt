package com.charurani.core.data.model

data class ResultOutput<out T>(val status: Status, val data: T?, val message: String?)