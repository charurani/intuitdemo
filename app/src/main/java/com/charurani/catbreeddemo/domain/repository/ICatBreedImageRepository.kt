package com.charurani.catbreeddemo.domain.repository

import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import io.reactivex.Observable

interface ICatBreedImageRepository {
    fun getData(catBreedId: String):Observable<List<CatBreedImageResponseEntity>>
}