package com.charurani.catbreeddemo.domain.usecase

import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import com.charurani.catbreeddemo.domain.model.request.ListCatBreedRequestData
import com.charurani.catbreeddemo.domain.repository.IListOfCatBreedRepository
import com.charurani.core.domain.UseCase
import com.charurani.core.networking.thread.PostExecutionThread
import io.reactivex.Observable
import java.util.concurrent.Executor
import javax.inject.Inject

class ListOfCatBreedsUseCase @Inject constructor(
    private val threadExecutor: Executor,
    private val postExecutionThread: PostExecutionThread,
    private val iListOfCatBreedRepository: IListOfCatBreedRepository
) : UseCase<List<CatBreedResponseEntity>, ListCatBreedRequestData>(
    threadExecutor,
    postExecutionThread
) {
    override fun buildUseCaseLiveData(params: ListCatBreedRequestData): Observable<List<CatBreedResponseEntity>> {
        return iListOfCatBreedRepository.provideListOfCatBreeds(params.limit, params.page)
    }
}