package com.charurani.catbreeddemo.domain.usecase

import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import com.charurani.catbreeddemo.domain.repository.ICatBreedImageRepository
import com.charurani.core.domain.UseCase
import com.charurani.core.networking.thread.PostExecutionThread
import io.reactivex.Observable
import java.util.concurrent.Executor
import javax.inject.Inject

class CatBreedImageUseCase
@Inject constructor(
    private val executor: Executor,
    private val postExecutionThread: PostExecutionThread,
    private val iCatBreedImageRepository: ICatBreedImageRepository
) :
    UseCase<List<CatBreedImageResponseEntity>, String>(executor, postExecutionThread) {
    override fun buildUseCaseLiveData(catBreedId: String): Observable<List<CatBreedImageResponseEntity>> {
        return iCatBreedImageRepository.getData(catBreedId)
    }
}