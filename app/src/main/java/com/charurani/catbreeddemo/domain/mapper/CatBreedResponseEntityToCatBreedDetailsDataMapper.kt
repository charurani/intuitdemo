package com.charurani.catbreeddemo.domain.mapper

import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import com.charurani.catbreeddemo.domain.model.CatBreedDetailsData
import javax.inject.Inject

class CatBreedResponseEntityToCatBreedDetailsDataMapper
@Inject constructor() {

    fun transform(catBreedResponseEntity: CatBreedResponseEntity): CatBreedDetailsData {
        return CatBreedDetailsData(
            catBreedResponseEntity.id,
            catBreedResponseEntity.name, catBreedResponseEntity.description,
            catBreedResponseEntity.imageUrl ?: "",
            catBreedResponseEntity.temperament
        )
    }

}