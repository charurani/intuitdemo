package com.charurani.catbreeddemo.domain.model.request

data class ListCatBreedRequestData(
    val limit: Int,
    val page: Int
)
