package com.charurani.catbreeddemo.domain.usecase

import com.charurani.catbreeddemo.domain.mapper.CatBreedResponseEntityToCatBreedDetailsDataMapper
import com.charurani.catbreeddemo.domain.model.CatBreedDetailsData
import com.charurani.catbreeddemo.domain.model.request.ListCatBreedRequestData
import com.charurani.core.domain.UseCase
import com.charurani.core.networking.thread.PostExecutionThread
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.Executor
import javax.inject.Inject

class ListOfCatBreedWithImageUseCase @Inject constructor(
    private val executor: Executor,
    private val postExecutionThread: PostExecutionThread,
    private val catBreedImageUseCase: CatBreedImageUseCase,
    private val listOfCatBreedsUseCase: ListOfCatBreedsUseCase,
    private val catBreedResponseEntityToCatBreedDetailsDataMapper: CatBreedResponseEntityToCatBreedDetailsDataMapper
) : UseCase<List<CatBreedDetailsData>, ListCatBreedRequestData>(executor, postExecutionThread) {

    override fun buildUseCaseLiveData(params: ListCatBreedRequestData): Observable<List<CatBreedDetailsData>> {
        val catBreedResponseEntitySubject: PublishSubject<List<CatBreedDetailsData>> =
            PublishSubject.create()
        val resultList = mutableListOf<CatBreedDetailsData>()
        disposables.add(
            listOfCatBreedsUseCase.execute(params = params)
                .subscribe(
                    { inputList ->
                        inputList?.forEach { catBreedResponseEntity ->
                            disposables.add(catBreedImageUseCase
                                .execute(catBreedResponseEntity.id)
                                .subscribe(
                                    { t ->
                                        catBreedResponseEntity.imageUrl = t?.get(0)?.url ?: ""
                                        resultList.add(catBreedResponseEntityToCatBreedDetailsDataMapper.transform(catBreedResponseEntity))
                                        if (resultList.size == inputList.size) {

                                            catBreedResponseEntitySubject.onNext(resultList)
                                            catBreedResponseEntitySubject.onComplete()
                                        }
                                    }
                                ) { t -> catBreedResponseEntitySubject.onError(t) }
                            )
                        }
                    }
                ) { t -> catBreedResponseEntitySubject.onError(t ?: Throwable()) }
        )
        return catBreedResponseEntitySubject.toFlowable(BackpressureStrategy.DROP).toObservable()
    }


}