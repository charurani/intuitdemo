package com.charurani.catbreeddemo.domain.repository

import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import io.reactivex.Observable

interface IListOfCatBreedRepository {
    fun provideListOfCatBreeds(limit: Int, page: Int): Observable<List<CatBreedResponseEntity>>
}