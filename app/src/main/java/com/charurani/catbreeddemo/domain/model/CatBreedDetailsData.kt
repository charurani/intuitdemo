package com.charurani.catbreeddemo.domain.model

data class CatBreedDetailsData (
    var id: String,
    var name: String,
    var description: String,
    var temperament: String,
    var imageUrl: String? = null
)