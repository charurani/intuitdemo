package com.charurani.catbreeddemo

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication

class CatBreedDemoApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }
}