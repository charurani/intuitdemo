package com.charurani.catbreeddemo.data.datasource

import com.charurani.catbreeddemo.data.api.CatBreedImage
import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import com.charurani.core.networking.NetworkClient
import io.reactivex.Observable
import javax.inject.Inject

class CatBreedImageNetworkDataSource @Inject constructor(

) {
    fun provideData(catBreedId: String): Observable<List<CatBreedImageResponseEntity>> {
        return NetworkClient.Companion.create(CatBreedImage::class.java)
            .getCatBreedImage(catBreedId)
    }
}