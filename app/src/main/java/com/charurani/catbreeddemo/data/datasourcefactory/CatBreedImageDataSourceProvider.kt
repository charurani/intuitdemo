package com.charurani.catbreeddemo.data.datasourcefactory

import com.charurani.catbreeddemo.data.datasource.CatBreedImageNetworkDataSource
import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import io.reactivex.Observable
import javax.inject.Inject

class CatBreedImageDataSourceProvider @Inject constructor(
    private val networkDataSource: CatBreedImageNetworkDataSource
) {

    fun provideData(catBreedId: String): Observable<List<CatBreedImageResponseEntity>> {
        return networkDataSource.provideData(catBreedId)
    }
}