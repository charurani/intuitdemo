package com.charurani.catbreeddemo.data.datasourcefactory

import com.charurani.catbreeddemo.data.datasource.ListOfCatBreedNetworkDataSource
import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import io.reactivex.Observable
import javax.inject.Inject

class ListOfCatBreedDataSourceProvider
@Inject constructor(
    private val networkDataSource: ListOfCatBreedNetworkDataSource
) {

    fun provideData(limit: Int, page: Int): Observable<List<CatBreedResponseEntity>> {
        return networkDataSource.provideData(limit, page)
    }
}