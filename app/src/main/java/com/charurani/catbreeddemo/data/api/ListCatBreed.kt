package com.charurani.catbreeddemo.data.api

import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ListCatBreed {

    @GET("breeds")
    fun getListOfCatBreeds(
        @Query("limit") limit: Int,
        @Query("page") page: Int
    ): Observable<List<CatBreedResponseEntity>>
}