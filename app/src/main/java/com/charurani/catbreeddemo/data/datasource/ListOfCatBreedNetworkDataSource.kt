package com.charurani.catbreeddemo.data.datasource

import com.charurani.catbreeddemo.data.api.ListCatBreed
import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import com.charurani.core.networking.NetworkClient
import io.reactivex.Observable
import javax.inject.Inject

class ListOfCatBreedNetworkDataSource @Inject constructor(

) {
    fun provideData(limit: Int, page: Int): Observable<List<CatBreedResponseEntity>> {
        return NetworkClient.Companion.create(ListCatBreed::class.java)
            .getListOfCatBreeds(limit, page)
    }
}