package com.charurani.catbreeddemo.data.entity

import com.squareup.moshi.Json

data class CatBreedImageResponseEntity(@Json(name = "url") val url: String) {
}