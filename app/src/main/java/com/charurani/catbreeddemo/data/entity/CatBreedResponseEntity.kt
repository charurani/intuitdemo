package com.charurani.catbreeddemo.data.entity

import com.squareup.moshi.Json

data class CatBreedResponseEntity(
    @Json(name = "id") val id: String,
    @Json(name = "name") val name: String,
    @Json(name = "description") val description: String,
    @Json(name = "temperament") val temperament: String,
    var imageUrl: String? = null
)