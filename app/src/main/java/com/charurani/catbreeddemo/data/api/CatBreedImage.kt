package com.charurani.catbreeddemo.data.api

import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CatBreedImage {

    @GET("images/search")
    fun getCatBreedImage(@Query("breed_ids") breedId: String): Observable<List<CatBreedImageResponseEntity>>
}