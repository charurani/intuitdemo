package com.charurani.catbreeddemo.data.repository

import com.charurani.catbreeddemo.data.datasourcefactory.ListOfCatBreedDataSourceProvider
import com.charurani.catbreeddemo.data.entity.CatBreedResponseEntity
import com.charurani.catbreeddemo.domain.repository.IListOfCatBreedRepository
import io.reactivex.Observable
import javax.inject.Inject

class ListOfCatBreedRepository @Inject constructor(
    private val listOfCatBreedDataSourceProvider: ListOfCatBreedDataSourceProvider
) : IListOfCatBreedRepository {
    override fun provideListOfCatBreeds(
        limit: Int,
        page: Int
    ): Observable<List<CatBreedResponseEntity>> {
        return listOfCatBreedDataSourceProvider.provideData(limit, page)
    }
}