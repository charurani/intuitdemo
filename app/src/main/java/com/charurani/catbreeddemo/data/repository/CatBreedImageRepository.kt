package com.charurani.catbreeddemo.data.repository

import com.charurani.catbreeddemo.data.datasourcefactory.CatBreedImageDataSourceProvider
import com.charurani.catbreeddemo.data.entity.CatBreedImageResponseEntity
import com.charurani.catbreeddemo.domain.repository.ICatBreedImageRepository
import io.reactivex.Observable
import javax.inject.Inject

class CatBreedImageRepository
@Inject constructor(
    private val catBreedImageDataSourceProvider: CatBreedImageDataSourceProvider
) : ICatBreedImageRepository {
    override fun getData(catBreedId: String): Observable<List<CatBreedImageResponseEntity>> {
        return catBreedImageDataSourceProvider.provideData(catBreedId)
    }

}