package com.charurani.catbreeddemo.di

import android.app.Activity
import com.charurani.catbreeddemo.CatBreedDemoApplication
import com.charurani.catbreeddemo.di.component.CBDActivityComponent
import com.charurani.catbreeddemo.di.component.CBDApplicationComponent
import com.charurani.catbreeddemo.di.component.DaggerCBDApplicationComponent
import com.charurani.catbreeddemo.di.module.CBDActivityModule
import com.charurani.catbreeddemo.di.module.CBDApplicationModule
import com.charurani.core.di.module.ActivityModule
import com.charurani.core.di.module.ApplicationModule

class AppDI {
    companion object {
        lateinit var applicationComponent: CBDApplicationComponent

        private fun getApplicationComponent(activity: Activity) : CBDApplicationComponent {
            applicationComponent = DaggerCBDApplicationComponent.builder()
                .cBDApplicationModule(CBDApplicationModule(activity.application as CatBreedDemoApplication))
                .applicationModule(ApplicationModule(activity.application))
                .build()
            return applicationComponent
        }

        fun getActivityComponent(activity: Activity): CBDActivityComponent {
            return getApplicationComponent(activity).plus(CBDActivityModule(activity), ActivityModule(activity))
        }
    }
}