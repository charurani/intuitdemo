package com.charurani.catbreeddemo.di.component

import com.charurani.catbreeddemo.di.module.CBDActivityModule
import com.charurani.catbreeddemo.di.module.CBDApplicationModule
import com.charurani.core.di.module.ActivityModule
import com.charurani.core.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [CBDApplicationModule::class, ApplicationModule::class])
interface CBDApplicationComponent {
    fun plus(
        cbdActivityModule: CBDActivityModule,
        activityModule: ActivityModule
    ): CBDActivityComponent
}