package com.charurani.catbreeddemo.di.component

import com.charurani.catbreeddemo.di.module.CBDActivityModule
import com.charurani.catbreeddemo.di.module.CBDViewModelModule
import com.charurani.catbreeddemo.view.catbreeddetails.CatBreedDetailsActivity
import com.charurani.catbreeddemo.view.catbreedslist.ListOfCatBreedActivity
import com.charurani.core.di.module.ActivityModule
import com.charurani.core.di.scopes.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [CBDActivityModule::class, ActivityModule::class, CBDViewModelModule::class])
interface CBDActivityComponent {
    fun inject(activity:ListOfCatBreedActivity)
    fun inject(activity:CatBreedDetailsActivity)
}