package com.charurani.catbreeddemo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.charurani.catbreeddemo.view.catbreeddetails.CatBreedDetailsViewModel
import com.charurani.catbreeddemo.view.catbreedslist.ListOfCatBreedViewModel
import com.charurani.core.di.viewmodel.ViewModelFactory
import com.charurani.core.di.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CBDViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListOfCatBreedViewModel::class)
    abstract fun bindIntoListingViewModel(listingViewModel: ListOfCatBreedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CatBreedDetailsViewModel::class)
    abstract fun bindIntoCatBreedDetailsViewModel(catBreedDetailsViewModel: CatBreedDetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

