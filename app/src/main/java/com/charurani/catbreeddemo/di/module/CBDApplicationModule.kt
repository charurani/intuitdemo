package com.charurani.catbreeddemo.di.module

import android.app.Application
import com.charurani.catbreeddemo.CatBreedDemoApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CBDApplicationModule(private val application: CatBreedDemoApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return application
    }
}