package com.charurani.catbreeddemo.di.module

import android.app.Activity
import com.charurani.catbreeddemo.data.repository.CatBreedImageRepository
import com.charurani.catbreeddemo.data.repository.ListOfCatBreedRepository
import com.charurani.catbreeddemo.domain.repository.ICatBreedImageRepository
import com.charurani.catbreeddemo.domain.repository.IListOfCatBreedRepository
import dagger.Module
import dagger.Provides

@Module
class CBDActivityModule(private val activity: Activity) {
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun provideIListOfCatBreedRepository(listOfCatBreedRepository: ListOfCatBreedRepository): IListOfCatBreedRepository {
        return listOfCatBreedRepository
    }

    @Provides
    fun provideICatBreedImageRepository(catBreedImageRepository: CatBreedImageRepository): ICatBreedImageRepository {
        return catBreedImageRepository
    }
}