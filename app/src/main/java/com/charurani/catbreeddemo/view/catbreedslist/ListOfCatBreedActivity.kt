package com.charurani.catbreeddemo.view.catbreedslist

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.charurani.catbreeddemo.R
import com.charurani.catbreeddemo.databinding.LayoutListCatBreedsBinding
import com.charurani.catbreeddemo.di.AppDI
import com.charurani.catbreeddemo.view.catbreeddetails.CatBreedDetailsActivity
import com.charurani.catbreeddemo.view.model.CatBreedDetailsModel
import kotlinx.android.synthetic.main.layout_list_cat_breeds.*
import kotlinx.android.synthetic.main.toolbar_layout_with_title.*
import javax.inject.Inject

class ListOfCatBreedActivity : AppCompatActivity(), ClickHandler {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ListOfCatBreedViewModel by viewModels { viewModelFactory }
    private lateinit var catBreedListAdapter: CatBreedListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: LayoutListCatBreedsBinding = DataBindingUtil.setContentView(
            this@ListOfCatBreedActivity,
            R.layout.layout_list_cat_breeds
        )
        binding.handler = this@ListOfCatBreedActivity
        AppDI.getActivityComponent(this).inject(this)
        initToolBar()
        setUpListAdapter()
        attachObserverForCatBreedList()
        attachObserverForError()
        getListOfCatBreeds()
    }

    private fun initToolBar() {
        setSupportActionBar(listing_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setUpListAdapter() {
        catBreedListAdapter = CatBreedListAdapter(this, this)
        listing_view.adapter = catBreedListAdapter
        listing_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        listing_view.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun attachObserverForCatBreedList() {
        viewModel.listOfCatBreedResponseLiveData.observe(this@ListOfCatBreedActivity,
            { catBreedList ->
                Log.e("charu", "data received")
                if (catBreedList != null && catBreedList.isNotEmpty()) {
                    showContent()
                }
                catBreedList?.let {
                    notifyListAdapter(catBreedList)
                }
            })
    }

    private fun attachObserverForError() {
        viewModel.errorLiveData.observe(this@ListOfCatBreedActivity,
            { error ->
                showErrors()
            })
    }

    private fun notifyListAdapter(listToBeDisplayed: List<CatBreedDetailsModel>) {
        catBreedListAdapter.catBreedList = listToBeDisplayed
        catBreedListAdapter.notifyDataSetChanged()
    }

    private fun showContent() {
        listing_view.visibility = View.VISIBLE
        error_layout.visibility = View.GONE
        listing_progressbar.visibility = View.GONE
    }

    private fun showProgress() {
        listing_progressbar.visibility = View.VISIBLE
        error_layout.visibility = View.GONE
        listing_view.visibility = View.GONE
    }

    private fun showErrors() {
        error_layout.visibility = View.VISIBLE
        listing_progressbar.visibility = View.GONE
        listing_view.visibility = View.GONE
    }

    override fun handleRetryButtonClick() {
        getListOfCatBreeds()
    }

    override fun handleListItemClick(position: Int) {
        viewModel.navigateToDetailsScreen(
            this@ListOfCatBreedActivity,
            CatBreedDetailsActivity::class.java,
            catBreedListAdapter.catBreedList[position]
        )
    }

    private fun getListOfCatBreeds() {
        showProgress()
        viewModel.getListOfCatBreeds()
    }

}