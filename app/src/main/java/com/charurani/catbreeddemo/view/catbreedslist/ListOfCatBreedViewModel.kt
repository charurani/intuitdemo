package com.charurani.catbreeddemo.view.catbreedslist

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.charurani.catbreeddemo.Constants
import com.charurani.catbreeddemo.domain.model.request.ListCatBreedRequestData
import com.charurani.catbreeddemo.domain.usecase.ListOfCatBreedWithImageUseCase
import com.charurani.catbreeddemo.view.catbreeddetails.CatBreedDetailsActivity
import com.charurani.catbreeddemo.view.mapper.CatBreedDetailsDataToCatBreedDetailsModelMapper
import com.charurani.catbreeddemo.view.model.CatBreedDetailsModel
import com.charurani.core.navigation.NavigationRouter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ListOfCatBreedViewModel @Inject constructor(
    val listOfCatBreedWithImageUseCase: ListOfCatBreedWithImageUseCase,
    val catBreedDetailsDataToCatBreedDetailsModelMapper: CatBreedDetailsDataToCatBreedDetailsModelMapper,
    var navigationRouter: NavigationRouter
) : ViewModel() {

    private var listOfCatBreedResponseMutableLiveData =
        MutableLiveData<List<CatBreedDetailsModel>>()
    val listOfCatBreedResponseLiveData: LiveData<List<CatBreedDetailsModel>>
        get() = listOfCatBreedResponseMutableLiveData

    private var errorMutableLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String>
        get() = errorMutableLiveData

    private var listOfCatBreedResponseExecutingStatusMutableLiveData =
        MutableLiveData<Boolean>(false)
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun getListOfCatBreeds() {
        if (listOfCatBreedResponseExecutingStatusMutableLiveData.value == false
            && listOfCatBreedResponseMutableLiveData.value == null
        ) {
            compositeDisposable.add(
                //todo add pagination logic
                listOfCatBreedWithImageUseCase.execute(ListCatBreedRequestData(Constants.PAGE_LIMIT_SIZE,0))
                    .subscribe(
                        { catBreedDetailsDataList ->
                            val catBreedDetailsModelList = mutableListOf<CatBreedDetailsModel>()
                            catBreedDetailsDataList.forEach {
                                catBreedDetailsModelList.add(
                                    catBreedDetailsDataToCatBreedDetailsModelMapper.transform(it)
                                )
                            }
                            listOfCatBreedResponseMutableLiveData.value = catBreedDetailsModelList
                            listOfCatBreedResponseExecutingStatusMutableLiveData.value = false
                        },
                        { throwable ->
                            errorMutableLiveData.value = throwable.localizedMessage
                            listOfCatBreedResponseExecutingStatusMutableLiveData.value = false
                        },
                        {
                            listOfCatBreedResponseExecutingStatusMutableLiveData.value = false
                        },
                        {
                            listOfCatBreedResponseExecutingStatusMutableLiveData.value = true
                        })
            )
        }
    }

    fun navigateToDetailsScreen(
        initiatingActivity: Activity,
        landingActivity: Class<CatBreedDetailsActivity>,
        catBreedDetailsModel: CatBreedDetailsModel
    ) {
        navigationRouter = navigationRouter.withParam(Constants.BUNDLE_KEY_CAT_BREED_DETAILS_MODEL, catBreedDetailsModel)!!
        navigationRouter.navigateTo(initiatingActivity, landingActivity)
    }
}