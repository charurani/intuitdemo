package com.charurani.catbreeddemo.view

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

object CustomBindingAdapter {
    @BindingAdapter("android:src")
    @JvmStatic
    fun setImageToImageView(imageView: ImageView, resourceUrl: String) {
        Glide.with(imageView.context)
            .load(resourceUrl)
            .apply(RequestOptions().onlyRetrieveFromCache(true).centerInside()).into(imageView)
    }
}