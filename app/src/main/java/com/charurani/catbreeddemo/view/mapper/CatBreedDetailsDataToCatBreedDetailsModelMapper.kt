package com.charurani.catbreeddemo.view.mapper

import com.charurani.catbreeddemo.domain.model.CatBreedDetailsData
import com.charurani.catbreeddemo.view.model.CatBreedDetailsModel
import javax.inject.Inject

class CatBreedDetailsDataToCatBreedDetailsModelMapper
@Inject constructor() {
    fun transform(catBreedDetailsData: CatBreedDetailsData): CatBreedDetailsModel {
        return CatBreedDetailsModel(
            catBreedDetailsData.id,
            catBreedDetailsData.name,
            catBreedDetailsData.description,
            catBreedDetailsData.imageUrl ?: "",
            catBreedDetailsData.temperament
        )
    }

}