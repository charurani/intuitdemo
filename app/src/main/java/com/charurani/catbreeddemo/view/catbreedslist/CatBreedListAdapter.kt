package com.charurani.catbreeddemo.view.catbreedslist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.charurani.catbreeddemo.R
import com.charurani.catbreeddemo.view.model.CatBreedDetailsModel
import kotlinx.android.synthetic.main.listing_cat_breed_cellview.view.*

class CatBreedListAdapter(
    val context: Context,
    val clickHandler: ClickHandler
) :
    RecyclerView.Adapter<CatBreedListAdapter.CatBreedListViewHolder>() {
    var catBreedList = listOf<CatBreedDetailsModel>()

    inner class CatBreedListViewHolder(
        val view: View
    ) : RecyclerView.ViewHolder(view) {

        fun bindData(catBreedDetailsModel: CatBreedDetailsModel) {
            view.cat_breed_name_textview.text = catBreedDetailsModel.name
            view.cat_breed_description_textview.text = catBreedDetailsModel.description
            Glide.with(context)
                .load(catBreedDetailsModel.imageUrl)
                .apply(RequestOptions().circleCrop())
                .into(view.findViewById(R.id.cat_breed_imageview))
            view.listing_cat_breed_cell_root.setOnClickListener { view ->
                clickHandler.handleListItemClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatBreedListViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.listing_cat_breed_cellview, parent, false)
        return CatBreedListViewHolder(view)
    }

    override fun onBindViewHolder(holder: CatBreedListViewHolder, position: Int) {
        holder.bindData(catBreedList.get(position))
    }

    override fun getItemCount(): Int {
        return catBreedList.size
    }
}