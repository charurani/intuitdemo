package com.charurani.catbreeddemo.view.catbreeddetails

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.charurani.catbreeddemo.Constants
import com.charurani.catbreeddemo.R
import com.charurani.catbreeddemo.databinding.LayoutCatBreedDetailsBinding
import com.charurani.catbreeddemo.di.AppDI
import com.charurani.catbreeddemo.view.model.CatBreedDetailsModel
import kotlinx.android.synthetic.main.toolbar_layout_with_title.*
import javax.inject.Inject

class CatBreedDetailsActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: CatBreedDetailsViewModel by viewModels { viewModelFactory }
    private lateinit var catBreedDetailsModel: CatBreedDetailsModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        receiveBundle()
        val binding: LayoutCatBreedDetailsBinding = DataBindingUtil.setContentView(
            this@CatBreedDetailsActivity,
            R.layout.layout_cat_breed_details
        )
        binding.catbreed = catBreedDetailsModel
        binding.executePendingBindings()
        AppDI.getActivityComponent(this).inject(this)
        initToolBar()
    }

    private fun initToolBar() {
        setSupportActionBar(listing_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar_title.text = catBreedDetailsModel.name
    }

    private fun receiveBundle() {
        catBreedDetailsModel =
            intent.extras?.getParcelable<CatBreedDetailsModel>(Constants.BUNDLE_KEY_CAT_BREED_DETAILS_MODEL)
                ?: CatBreedDetailsModel("", "", "", "", "")
    }
}