package com.charurani.catbreeddemo.view.catbreedslist

interface ClickHandler {
    fun handleRetryButtonClick()
    fun handleListItemClick(position: Int)
}