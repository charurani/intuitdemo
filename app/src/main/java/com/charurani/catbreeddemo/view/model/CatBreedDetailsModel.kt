package com.charurani.catbreeddemo.view.model

import android.os.Parcel
import android.os.Parcelable


data class CatBreedDetailsModel(
    var id: String,
    var name: String,
    var description: String,
    var temperament: String,
    var imageUrl: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(temperament)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CatBreedDetailsModel> {
        override fun createFromParcel(parcel: Parcel): CatBreedDetailsModel {
            return CatBreedDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<CatBreedDetailsModel?> {
            return arrayOfNulls(size)
        }
    }

}