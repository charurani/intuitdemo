package com.charurani.catbreeddemo

interface Constants {
    companion object Constant{
        val BUNDLE_KEY_CAT_BREED_DETAILS_MODEL = "cat_breed_details_model"
        val PAGE_LIMIT_SIZE = 10
    }
}